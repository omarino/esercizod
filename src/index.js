import { women } from "./functions/expression.js"; 

let form = document.querySelector("form[name='colpa']");
form.addEventListener('input',function(e) {
    e.preventDefault();
    let donna = document.querySelector("input[name='donna']");
    message(donna);

});

const message = (e) => {
    // let b = document.createElement('div');
    // b.innerText = 'la tua percentuale di colpa è ' + women(e.value);
    // e.parentElement.appendChild(b);

    // let x = e.nextElementSibling;

    // if(x) {
    //     x.remove();
    // }

    if (e.value < 101 && e.value >= 0) {
        const calcolo = document.getElementById('calcolo'); 
        calcolo.innerHTML = 'la tua percentuale di colpa è ' + women(e.value); 
        return true
    } else {
        e.value = '';
        const calcolo = document.getElementById('calcolo');
        calcolo.innerHTML = '';
        return false;
    }
}

